class AddColumnsToReference < ActiveRecord::Migration[5.1]
  def change
    add_column :references, :authorName, :string
    add_column :references, :authorLastName, :String
  end
end
