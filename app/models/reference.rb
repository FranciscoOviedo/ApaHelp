# == Schema Information
#
# Table name: references
#
#  id               :integer          not null, primary key
#  user_id          :integer
#  year             :integer
#  title            :string
#  subtitle         :string
#  publicationPlace :string
#  editorial        :string
#  pageNumber       :integer
#  referencedText   :text
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  authorName       :string
#  authorLastName   :String
#

class Reference < ApplicationRecord
  belongs_to :user
end
