require 'test_helper'

class InformationControllerTest < ActionDispatch::IntegrationTest
  test "should get NormasAPA" do
    get information_NormasAPA_url
    assert_response :success
  end

  test "should get ListasyFiguras" do
    get information_ListasyFiguras_url
    assert_response :success
  end

  test "should get Citas" do
    get information_Citas_url
    assert_response :success
  end

  test "should get Referencias" do
    get information_Referencias_url
    assert_response :success
  end

end
