class CreateReferences < ActiveRecord::Migration[5.1]
  def change
    create_table :references do |t|
      t.references :user, foreign_key: true
      t.integer :year
      t.string :title
      t.string :subtitle
      t.string :publicationPlace
      t.string :editorial
      t.integer :pageNumber
      t.text :referencedText

      t.timestamps
    end
  end
end
