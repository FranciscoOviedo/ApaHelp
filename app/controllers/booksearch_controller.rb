class BooksearchController < ApplicationController
    include HTTParty
    base_uri "https://www.googleapis.com/books/v1/"
    
    def search
        #self.class.get('/volumes?q=cien anos de soledad')

        #url = 'https://www.googleapis.com/books/v1/volumes?q=el zahir'
        url = 'https://www.googleapis.com/books/v1/volumes?q='+params[:search]
        
        response = HTTParty.get(url, :verify => false)
        @response = JSON.parse(response.body)['items']
    end
end