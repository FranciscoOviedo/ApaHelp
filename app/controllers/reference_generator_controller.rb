class ReferenceGeneratorController < ApplicationController
  layout 'application', :except => :action
  layout "main", :only => :index
  
  def index
  end

  def new
    @reference = Reference.new
  end
end
