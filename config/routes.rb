Rails.application.routes.draw do
  resources :books
  devise_for :users
  get 'information/NormasAPA'
  get '/booksearch', to: 'booksearch#search', as: :booksearch_path

  root 'reference_generator#index'
  resources :references
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
