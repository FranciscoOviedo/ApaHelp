json.extract! reference, :id, :user_id, :year, :title, :subtitle, :publicationPlace, :editorial, :pageNumber, :created_at, :updated_at
json.url reference_url(reference, format: :json)
